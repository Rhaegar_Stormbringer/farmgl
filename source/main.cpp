#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPES 1
#include <GL3/gl3.h>

#endif // WIN32

#include <SDL2/SDL.h>
#include <iostream>

#include "SceneOpenGL.h"

int main(int argc, char **argv)
{
    // Creation scene
    SceneOpenGL scene("FarmGL", 800, 600);

    // Initialisation scene
    if (scene.initialiserFenetre() == false)
    {
        return -1;
    }

    if (scene.initGL() == false)
    {
        return -1;
    }

    // Boucle principale
    scene.bouclePrincipale();

    return 0;
}
