#ifndef DEF_SCENEOPENGL
#define DEF_SCENEOPENGL

#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPES 1
#include <GL3/gl3.h>

#endif // WIN32

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <SDL2/SDL.h>
#include <iostream>
#include <string>

#include "Shader.h"
#include "Input.h"
#include "Camera.h"
#include "Texture.h"

#include "Cabane.h"
#include "Caisse.h"
#include "Cristal.h"
#include "Sol.h"

class SceneOpenGL
{
    public:
        SceneOpenGL(std::string titreFenetre, int largeurFenetre, int hauteurFenetre);
        ~SceneOpenGL();

        bool initialiserFenetre();
        void bouclePrincipale();
        bool initGL();

    private:
        std::string m_titreFenetre;
        int m_largeurFenetre;
        int m_hauteurFenetre;

        SDL_Window* m_fenetre;
        SDL_GLContext m_contexteOpenGL;
        SDL_Event m_evenements;

        Input m_input;
};
#endif // DEF_SCENEOPENGL
