#ifndef DEF_PLANE
#define DEF_PLANE

// Includes OpenGL

#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPE 1
#include <GL3/gl3.h>

#endif // WIN32

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Includes

#include "Shader.h"

class Plane
{
    public:
        Plane(float taille, std::string const vertexShader, std::string const fragmentShader);
        ~Plane();

        void afficher(glm::mat4 &projection, glm::mat4 &modelview);

    protected:
        Shader m_shader;
        float m_vertices[108];
        float m_couleurs[108];
};

#endif // DEF_PLANE
