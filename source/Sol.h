#ifndef DEF_SOL
#define DEF_SOL

// Includes OpenGL

#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPE 1
#include <GL3/gl3.h>

#endif // WIN32

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Includes

#include "Shader.h"
#include "Texture.h"
#include <string>

class Sol
{
    public:
        Sol(float taille, std::string const vertexShader, std::string const fragmentShader, std::string const texture, float repetition);
        ~Sol();
        void afficher(glm::mat4 &projection, glm::mat4 &modelview);

    private:
        Shader m_shader;
        Texture m_texture;
        float m_vertices[18];
        float m_coordsTexture[12];

};

#endif

