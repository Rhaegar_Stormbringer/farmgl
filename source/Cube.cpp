#include "Cube.h"
using namespace glm;

Cube::Cube(float taille, std::string const vertexShader, std::string const fragmentShader) : m_shader(vertexShader, fragmentShader)
{
    // Chargement shader
    m_shader.charger();

    // Tableau vertices & couleurs

    taille /= 2;

    // Vertices
    float verticesTmp[] = {/* FACE FOND */
                    -taille, -taille, -taille,       // vertex 0
                        taille, -taille, -taille,    // vertex 1
                        taille, taille, -taille,     // vertex 2

                    -taille, -taille, -taille,       // vertex 0
                        -taille, taille, -taille,    // vertex 3
                        taille, taille, -taille,     // vertex 2

                    /* FACE DROITE */
                    taille, -taille, taille,         // vertex 5
                        taille, -taille, -taille,    // vertex 1
                        taille, taille, -taille,     // vertex 2

                    taille, -taille, taille,         // vertex 5
                        taille, taille, taille,      // vertex 6
                        taille, taille, -taille,     // vertex 2

                    /* FACE BAS */
                    -taille, -taille, -taille,       // vertex 0
                        taille, -taille, -taille,    // vertex 1
                        -taille, -taille, taille,    // vertex 4

                    taille, -taille, -taille,        // vertex 1
                        taille, -taille, taille,     // vertex 5
                        -taille, -taille, taille,    // vertex 4

                    /* FACE GAUCHE */
                    -taille, -taille, -taille,       // vertex 0
                        -taille, taille, -taille,    // vertex 3
                        -taille, -taille, taille,    // vertex 4

                     -taille, -taille, taille,       // vertex 4
                        -taille, taille, taille,     // vertex 7
                        -taille, taille, -taille,    // vertex 3

                    /* FACE DEVANT */
                    -taille, taille, taille,         // vertex 7
                        taille, taille, taille,      // vertex 6
                        -taille, -taille, taille,    // vertex 4

                    -taille, -taille, taille,        // vertex 4
                        taille, taille, taille,      // vertex 6
                        taille, -taille, taille,     // vertex 5

                    /* FACE HAUT */
                    taille, taille, taille,          // vertex 6
                        -taille, taille, taille,     // vertex 7
                        taille, taille, -taille,     // vertex 2

                    -taille, taille, taille,         // vertex 7
                        taille, taille, -taille,     // vertex 2
                        -taille, taille, -taille     // vertex 3
                    };

    // Couleurs
    float couleursTmp[] = {/* FACE FOND (rouge) */
                        1.0, 0.0, 0.0,
                            1.0, 0.0, 0.0,
                            1.0, 0.0, 0.0,  // Triangle 1

                        1.0, 0.0, 0.0,
                            1.0, 0.0, 0.0,
                            1.0, 0.0, 0.0,  // Triangle 2

                        /* FACE DROITE (vert) */
                        0.0, 1.0, 0.0,
                            0.0, 1.0, 0.0,
                            0.0, 1.0, 0.0,  // Triangle 1

                        0.0, 1.0, 0.0,
                            0.0, 1.0, 0.0,
                            0.0, 1.0, 0.0,  // Triangle 2

                        /* FACE BAS (bleu) */
                        0.0, 0.0, 1.0,
                            0.0, 0.0, 1.0,
                            0.0, 0.0, 1.0,  // Triangle 1

                        0.0, 0.0, 1.0,
                            0.0, 0.0, 1.0,
                            0.0, 0.0, 1.0, // Triangle 2

                        /* FACE GAUCHE (rose) */
                        1.0, 0.0, 1.0,
                            1.0, 0.0, 1.0,
                            1.0, 0.0, 1.0,  // Triangle 1

                        1.0, 0.0, 1.0,
                            1.0, 0.0, 1.0,
                            1.0, 0.0, 1.0,  // Triangle 2

                        /* FACE DEVANT (jaune) */
                        1.0, 1.0, 0.0,
                            1.0, 1.0, 0.0,
                            1.0, 1.0, 0.0,  // Triangle 1

                        1.0, 1.0, 0.0,
                            1.0, 1.0, 0.0,
                            1.0, 1.0, 0.0,  // Triangle 2

                        /* FACE HAUT (cyan) */
                        0.0, 1.0, 1.0,
                            0.0, 1.0, 1.0,
                            0.0, 1.0, 1.0,  // Triangle 1

                        0.0, 1.0, 1.0,
                            0.0, 1.0, 1.0,
                            0.0, 1.0, 1.0  // Triangle 2
                        };

        for (int i(0); i < 108; i++)
        {
            m_vertices[i] = verticesTmp[i];
            m_couleurs[i] = couleursTmp[i];
        }
}

Cube::~Cube(){}

void Cube::afficher(mat4 &projection, mat4 &modelview)
{
    /* ------ Activation shader ------ */
    glUseProgram(m_shader.getProgramID());

        // Envoi vertices
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, m_vertices);
        glEnableVertexAttribArray(0);

        // Envoi couleur
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, m_couleurs);
        glEnableVertexAttribArray(1);

        // Envoi matrices
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "projection"), 1, GL_FALSE, value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelview"), 1, GL_FALSE, value_ptr(modelview));


        // DESSIN
        glDrawArrays(GL_TRIANGLES, 0, 36);

        // Desactivation tableaux
        glDisableVertexAttribArray(1),
        glDisableVertexAttribArray(0);

    /* ---- Desactivation shader ----- */
    glUseProgram(0);
}
