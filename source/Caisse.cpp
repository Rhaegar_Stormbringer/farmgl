#include "Caisse.h"
using namespace glm;

Caisse::Caisse(float taille, std::string const vertexShader, std::string const fragmentShader, std::string const texture) : m_shader(vertexShader, fragmentShader), m_texture(texture), m_vboID(0), m_tailleVerticesBytes(108*sizeof(float)), m_tailleTextureBytes(72*sizeof(float)), m_vaoID(0)
{
    // Chargement shader
    m_shader.charger();

    // Chargement texture
    m_texture.charger();

    taille /= 2;

    // Vertices
    float verticesTmp[] = {/* FACE FOND */
                    -taille, -taille, -taille,       // vertex 0
                        taille, -taille, -taille,    // vertex 1
                        taille, taille, -taille,     // vertex 2

                    -taille, -taille, -taille,       // vertex 0
                        -taille, taille, -taille,    // vertex 3
                        taille, taille, -taille,     // vertex 2

                    /* FACE DROITE */
                    taille, -taille, taille,         // vertex 5
                        taille, -taille, -taille,    // vertex 1
                        taille, taille, -taille,     // vertex 2

                    taille, -taille, taille,         // vertex 5
                        taille, taille, taille,      // vertex 6
                        taille, taille, -taille,     // vertex 2

                    /* FACE BAS */
                    -taille, -taille, -taille,       // vertex 0
                        taille, -taille, -taille,    // vertex 1
                        -taille, -taille, taille,    // vertex 4

                    taille, -taille, -taille,        // vertex 1
                        taille, -taille, taille,     // vertex 5
                        -taille, -taille, taille,    // vertex 4

                    /* FACE GAUCHE */
                    -taille, -taille, -taille,       // vertex 0
                        -taille, taille, -taille,    // vertex 3
                        -taille, -taille, taille,    // vertex 4

                     -taille, -taille, taille,       // vertex 4
                        -taille, taille, taille,     // vertex 7
                        -taille, taille, -taille,    // vertex 3

                    /* FACE DEVANT */
                    -taille, taille, taille,         // vertex 7
                        taille, taille, taille,      // vertex 6
                        -taille, -taille, taille,    // vertex 4

                    -taille, -taille, taille,        // vertex 4
                        taille, taille, taille,      // vertex 6
                        taille, -taille, taille,     // vertex 5

                    /* FACE HAUT */
                    taille, taille, taille,          // vertex 6
                        -taille, taille, taille,     // vertex 7
                        taille, taille, -taille,     // vertex 2

                    -taille, taille, taille,         // vertex 7
                        taille, taille, -taille,     // vertex 2
                        -taille, taille, -taille     // vertex 3
                    };

    // Coordonnees texture
    float coordTextureTmp[] = {/*Face 1*/
                               0, 0,   1, 0,   1, 1,
                               0, 0,   0, 1,   1, 1,
                               /*Face 2*/
                               0, 0,   1, 0,   1, 1,
                               0, 0,   0, 1,   1, 1,
                               /*Face 3*/
                               0, 0,   1, 0,   1, 1,
                               0, 0,   0, 1,   1, 1,
                               /*Face 4*/
                               0, 0,   1, 0,   1, 1,
                               0, 0,   0, 1,   1, 1,
                               /*Face 5*/
                               0, 0,   1, 0,   1, 1,
                               0, 0,   0, 1,   1, 1,
                               /*Face 6*/
                               0, 0,   1, 0,   1, 1,
                               0, 0,   0, 1,   1, 1,
                               };

    for (int i(0); i < 108; i++)
    {
        m_vertices[i] = verticesTmp[i];
    }
    for(int i(0); i<72; i++)
    {
        m_coordsTexture[i] = coordTextureTmp[i];
    }

    chargerVBO();
    chargerVAO();
}

Caisse::~Caisse()
{
    glDeleteBuffers(1, &m_vboID);
    glDeleteVertexArrays(1, &m_vaoID);
}

void Caisse::chargerVBO()
{
    // Destruction eventuel ancien VBO (fuite memoire)
    if (glIsBuffer(m_vboID) == GL_TRUE)
    {
        glDeleteBuffers(1, &m_vboID);
    }

    // Generation ID
    glGenBuffers(1, &m_vboID);

    // Verouillage VBO
    glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

    // Allocation memoire VRAM
    glBufferData(GL_ARRAY_BUFFER, m_tailleTextureBytes+m_tailleVerticesBytes, 0, GL_STATIC_DRAW);

    // Transfert data
    glBufferSubData(GL_ARRAY_BUFFER, 0, m_tailleVerticesBytes, m_vertices);
    glBufferSubData(GL_ARRAY_BUFFER, m_tailleVerticesBytes, m_tailleTextureBytes, m_coordsTexture);

    // Deverouillage VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Caisse::chargerVAO()
{
    // Destruction eventuel ancien VAO (fuite memoire)
    if (glIsVertexArray(m_vaoID) == GL_TRUE)
    {
        glDeleteVertexArrays(1, &m_vaoID);
    }

    // Generation ID
    glGenVertexArrays(1, &m_vaoID);

    // Verrouillage VAO
    glBindVertexArray(m_vaoID);

        // Verrouillage VBO
        glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

            // Envoi vertices
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
            glEnableVertexAttribArray(0);

            // Envoi texture
            glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(m_tailleVerticesBytes));
            glEnableVertexAttribArray(2);

        // Deverrouillage VBO
        glBindBuffer(GL_ARRAY_BUFFER, 0);

    // Deverrouillage VAO
    glBindVertexArray(0);
}

void Caisse::updateVBO(void *data, int tailleBytes, int decalage)
{
    // Verrouillage VBO
    glBindBuffer(GL_ARRAY_BUFFER, m_vboID);

        // Recuperation adresse VBO
        void *adresseVBO = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);

        if (adresseVBO == NULL)
        {
            std::cout << "Erreur recuperation VBO" << std::endl;
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            return;
        }

    // MAJ data
    memcpy((char*)adresseVBO+decalage, data, tailleBytes);

    // Annulation pointeur
    glUnmapBuffer(GL_ARRAY_BUFFER);
    adresseVBO = 0;

    // Deverrouillage VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Caisse::afficher(mat4 &projection, mat4 &modelview)
{
    // Activation shader
    glUseProgram(m_shader.getProgramID());

        // Verrouillage VAO
        glBindVertexArray(m_vaoID);

            // Envoi matrices
            glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "projection"), 1, GL_FALSE, value_ptr(projection));
            glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelview"), 1, GL_FALSE, value_ptr(modelview));

            // Verouillage texture
            glBindTexture(GL_TEXTURE_2D, m_texture.getID());

                // Rendu
                glDrawArrays(GL_TRIANGLES, 0, 36);

            // Deverouillage texture
            glBindTexture(GL_TEXTURE_2D, 0);

        // Deverrouillage VAO
        glBindVertexArray(0);

        // Desactivation tableaux (inutile -> VAO)
        //glDisableVertexAttribArray(2);
        //glDisableVertexAttribArray(0);

    // Desactivation shader
    glUseProgram(0);
}
