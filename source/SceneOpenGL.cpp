#include "SceneOpenGL.h"
using namespace glm;

SceneOpenGL::SceneOpenGL(std::string titreFenetre, int largeurFenetre, int hauteurFenetre) : m_titreFenetre(titreFenetre), m_largeurFenetre(largeurFenetre), m_hauteurFenetre(hauteurFenetre), m_fenetre(0), m_contexteOpenGL(0), m_input()
{}

SceneOpenGL::~SceneOpenGL()
{
    SDL_GL_DeleteContext(m_contexteOpenGL);
    SDL_DestroyWindow(m_fenetre);
    SDL_Quit();
}

bool SceneOpenGL::initialiserFenetre()
{
    // Initialisation de la SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << std::endl;
        SDL_Quit();

        return false;
    }

    // Version d'OpenGL
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    //SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // Double Buffer
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    // Creation de la fenetre
    m_fenetre = SDL_CreateWindow(m_titreFenetre.c_str(),
                                 SDL_WINDOWPOS_CENTERED,
                                 SDL_WINDOWPOS_CENTERED,
                                 m_largeurFenetre,
                                 m_hauteurFenetre,
                                 SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

    if (m_fenetre == 0)
    {
        std::cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << std::endl;
        SDL_Quit();

        return false;
    }

    // Creation contexte OpenGL
    m_contexteOpenGL = SDL_GL_CreateContext(m_fenetre);

    if (m_contexteOpenGL == 0)
    {
        std::cout << SDL_GetError() << std::endl;
        SDL_DestroyWindow(m_fenetre);
        SDL_Quit();

        return false;
    }

    return true;
}

bool SceneOpenGL::initGL()
{
    #ifdef WIN32

    // Initialisation GLEW
    GLenum initialisationGLEW(glewInit());

    if (initialisationGLEW != GLEW_OK)
    {
        std::cout << "Erreur d'initialisation de GLEW : " << glewGetErrorString(initialisationGLEW) << std::endl;

        SDL_GL_DeleteContext(m_contexteOpenGL);
        SDL_DestroyWindow(m_fenetre);
        SDL_Quit();

        return false;
    }
    #endif // WIN32

    // Activation Depth Buffer
    glEnable(GL_DEPTH_TEST);

    return true;
}


void SceneOpenGL::bouclePrincipale()
{
    unsigned int frameRate(1000/60);
    Uint32 debutBoucle(0), finBoucle(0), tempsEcoule(0);


    // Matrices
    mat4 projection;
    mat4 modelview;
    mat4 sauvergardeModelview;

    projection = perspective(70.0, (double)m_largeurFenetre / m_hauteurFenetre, 1.0, 100.0);
    modelview = mat4(1.0);

    // Camera
    Camera camera(vec3(15, 15, 15), vec3(0, 0, 0), vec3(0, 1, 0), 0.5, 0.5);

    // Capture pointeur
    m_input.afficherPointeur(false);
    m_input.capturerPointeur(true);

    /*** ----- Modeles 3D ----- ***/

    /** -- Sol -- **/
    Sol herbe(30.0, "Shaders/texture.vert", "Shaders/texture.frag", "Textures/veg005.jpg", 15.0);
    Sol terre(10.0, "Shaders/texture.vert", "Shaders/texture.frag", "Textures/ground014.jpg", 5.0);
    /** --------- **/

    /** -- Cabane -- **/
    Cabane cabane(20.0, "Shaders/texture.vert", "Shaders/texture.frag", "Textures/wall01.jpg", "Textures/roof01.jpg");
    /** ------------ **/

    /** -- Caisse -- **/
    Caisse caisse1(4.0, "Shaders/texture.vert", "Shaders/texture.frag", "Textures/crate12.jpg");
    Caisse caisse2(4.0, "Shaders/texture.vert", "Shaders/texture.frag", "Textures/crate13.jpg");
    Caisse caisse3(4.0, "Shaders/texture.vert", "Shaders/texture.frag", "Textures/crate14.jpg");
    /** ------------ **/

    /** -- Cristal -- **/
    Cristal cristal(2.0, "Shaders/texture.vert", "Shaders/texture.frag", "Textures/crystal1.tga");
    /** ------------ **/


    /*** ---------------------- ***/
    // Boucle principale

    while(!m_input.terminer())
    {
    // On d�finit le temps de d�but de boucle

    debutBoucle = SDL_GetTicks();


    // Gestion des �v�nements

    m_input.updateEvenements();

    if(m_input.getTouche(SDL_SCANCODE_ESCAPE))
       break;

    // Gestion Camera 1
    camera.deplacer(m_input);

    // Nettoyage de l'�cran

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    // Gestion camera 2
    camera.lookAt(modelview);

    // Placement de la cam�ra
    //modelview = lookAt(vec3(3, 3, 3), vec3(0, 0, 0), vec3(0, 1, 0));



    /*** --- RENDU --- ***/

    // SAUVEGARDE REPERE INITIAL
    sauvergardeModelview = modelview;

        // Placement des sols
        terre.afficher(projection, modelview);
        // (!) Abaissement du repere (pour eviter une superposition de textures)
        modelview = translate(modelview, vec3(0, -0.01, 0));
        herbe.afficher(projection, modelview);
    modelview = sauvergardeModelview;

        // Placement de la cabane
        cabane.afficher(projection, modelview);
    modelview = sauvergardeModelview;
        // Placement des caisses
        // (!) Soulevement du repere (pour avoir les caisses par dessus le sol)
        modelview = translate(modelview, vec3(0, 2, 4));
        caisse1.afficher(projection, modelview);
    modelview = sauvergardeModelview;
        modelview = translate(modelview, vec3(5, 2, -3));
        caisse2.afficher(projection, modelview);
    modelview = sauvergardeModelview;
        modelview = translate(modelview, vec3(-5, 2, -5));
        caisse3.afficher(projection, modelview);
    modelview = sauvergardeModelview;

        // Placement du cristal
        // (!) Soulevement du repere (pour avoir le cristal par dessus le sol et les caisses)
        modelview = translate(modelview, vec3(0, 6, 4));
        modelview = rotate(modelview, cristal.calculRotation(), vec3(0, 1, 0));
        cristal.afficher(projection, modelview);
    modelview = sauvergardeModelview;


    /*** ------------- ***/

    // Actualisation de la fen�tre

    SDL_GL_SwapWindow(m_fenetre);


    // Calcul du temps �coul�

    finBoucle = SDL_GetTicks();
    tempsEcoule = finBoucle - debutBoucle;


    // Si n�cessaire, on met en pause le programme

    if(tempsEcoule < frameRate)
        SDL_Delay(frameRate - tempsEcoule);
    }
}
