#ifndef DEF_CRISTAL
#define DEF_CRISTAL

// Includes OpenGL

#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPE 1
#include <GL3/gl3.h>

#endif // WIN32

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Includes

// Include
#include "Shader.h"
#include "Texture.h"
#include <string>

class Cristal
{
    public:
        Cristal(float taille, std::string const vertexShader, std::string const fragmentShader, std::string const texture);
        ~Cristal();
        void afficher(glm::mat4 &projection, glm::mat4 &modelview);
        float calculRotation();

    private:
        float m_angle;
        Shader m_shader;
        float m_vertices[72];
        Texture m_texture;
        float m_coordsTexture[48];

};

#endif
