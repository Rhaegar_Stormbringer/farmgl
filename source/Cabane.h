#ifndef DEF_CABANE
#define DEF_CABANE

// Includes OpenGL

#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPE 1
#include <GL3/gl3.h>

#endif // WIN32

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Includes

// Include
#include "Shader.h"
#include "Texture.h"
#include <string>

class Cabane
{
    public:
        Cabane(float taille, std::string const vertexShader, std::string const fragmentShader, std::string const texture_mur, std::string const texture_toit);
        ~Cabane();
        void afficher(glm::mat4 &projection, glm::mat4 &modelview);

    private:
        Shader m_shader;
        float m_vertices[108];
        Texture m_texture_mur;
        Texture m_texture_toit;
        float m_coordsTexture[72];

};

#endif

