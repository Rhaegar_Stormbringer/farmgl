#include "Sol.h"
using namespace glm;

Sol::Sol(float taille, std::string const vertexShader, std::string const fragmentShader, std::string const texture, float repetition) : m_shader(vertexShader, fragmentShader), m_texture(texture)
{
    // Chargement shader
    m_shader.charger();

    // Chargement texture
    m_texture.charger();

    float verticesTmp[] = {/* FACE */
                    -taille, 0, -taille,
                        taille, 0, -taille,
                        taille, 0, taille,  // Triangle 1

                    -taille, 0, -taille,
                        -taille, 0, taille,
                        taille, 0, taille   // Triangle 2
                    };

    // Coordonnees texture
    float coordTextureTmp[] = {/*Triange 1&2 */
                               0, 0,   1*repetition, 0,   1*repetition, 1*repetition,
                               0, 0,   0, 1*repetition,   1*repetition, 1*repetition
                               };

    for (int i(0); i<18; i++)
    {
        m_vertices[i] = verticesTmp[i];
    }
    for(int i(0); i<12; i++)
    {
        m_coordsTexture[i] = coordTextureTmp[i];
    }
}

Sol::~Sol(){}

void Sol::afficher(mat4 &projection, mat4 &modelview)
{
    // Activation shader
    glUseProgram(m_shader.getProgramID());

        // Envoi vertices
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, m_vertices);
        glEnableVertexAttribArray(0);

        // Envoi texture
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, m_coordsTexture);
        glEnableVertexAttribArray(2);

        // Envoi matrices
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "projection"), 1, GL_FALSE, value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelview"), 1, GL_FALSE, value_ptr(modelview));

        // Verouillage texture
        glBindTexture(GL_TEXTURE_2D, m_texture.getID());

        // Rendu
        glDrawArrays(GL_TRIANGLES, 0, 6);

        // Deverouillage texture
        glBindTexture(GL_TEXTURE_2D, 0);

        // Desactivation tableaux
        glDisableVertexAttribArray(2);
        glDisableVertexAttribArray(0);

    // Desactivation shader
    glUseProgram(0);
}

