#ifndef DEF_CAISSE
#define DEF_CAISSE

// Includes OpenGL

#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPE 1
#include <GL3/gl3.h>

#endif // WIN32

// Includes GLM

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Includes

// Include
#include "Shader.h"
#include "Texture.h"
#include <string>
#include <cstring>

// Macro VBO
#ifndef BUFFER_OFFSET
#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))
#endif // BUFFER_OFFSET

class Caisse
{
    public:
        Caisse(float taille, std::string const vertexShader, std::string const fragmentShader, std::string const texture);
        ~Caisse();
        void chargerVBO();
        void chargerVAO();
        void updateVBO(void *data, int tailleBytes, int decalage);
        void afficher(glm::mat4 &projection, glm::mat4 &modelview);

    private:
        Shader m_shader;
        float m_vertices[108];
        Texture m_texture;
        float m_coordsTexture[72];

        GLuint m_vboID;
        int m_tailleVerticesBytes;
        int m_tailleTextureBytes;

        GLuint m_vaoID;

};

#endif
