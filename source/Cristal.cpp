#include "Cristal.h"
using namespace glm;

Cristal::Cristal(float taille, std::string const vertexShader, std::string const fragmentShader, std::string const texture) : m_angle(0.0), m_shader(vertexShader, fragmentShader), m_texture(texture)
{
    // Chargement shader
    m_shader.charger();

    // Chargement texture
    m_texture.charger();

    taille /= 2;

    // Vertices
    float verticesTmp[] = {/* TRIANGLE 1 */
                    0, taille, 0,                       // vertex pic haut
                        0.5f*taille, 0, -0.5f*taille,   // vertex carre droite haut
                        0.5f*taille, 0, 0.5f*taille,    // vertex carre droite bas

                    /* TRIANGLE 2 */
                    0, taille, 0,                       // vertex pic haut
                        0.5f*taille, 0, -0.5f*taille,   // vertex carre droite haut
                        -0.5f*taille, 0, -0.5f*taille,  // vertex carre gauche haut

                    /* TRIANGLE 3 */
                    0, taille, 0,                       // vertex pic haut
                        -0.5f*taille, 0, -0.5f*taille,  // vertex carre gauche haut
                        -0.5f*taille, 0, 0.5f*taille,   // vertex carre gauche bas

                    /* TRIANGLE 4 */
                    0, taille, 0,                       // vertex pic haut
                        0.5f*taille, 0, 0.5f*taille,    // vertex carre droite bas
                        -0.5f*taille, 0, 0.5f*taille,   // vertex carre gauche bas

                    /* TRIANGLE 5 */
                    0, -taille, 0,                       // vertex pic bas
                        0.5f*taille, 0, -0.5f*taille,   // vertex carre droite haut
                        0.5f*taille, 0, 0.5f*taille,    // vertex carre droite bas

                    /* TRIANGLE 6 */
                    0, -taille, 0,                       // vertex pic bas
                        0.5f*taille, 0, -0.5f*taille,   // vertex carre droite haut
                        -0.5f*taille, 0, -0.5f*taille,  // vertex carre gauche haut

                    /* TRIANGLE 7 */
                    0, -taille, 0,                       // vertex pic bas
                        -0.5f*taille, 0, -0.5f*taille,  // vertex carre gauche haut
                        -0.5f*taille, 0, 0.5f*taille,   // vertex carre gauche bas

                    /* TRIANGLE 8 */
                    0, -taille, 0,                       // vertex pic bas
                        0.5f*taille, 0, 0.5f*taille,    // vertex carre droite bas
                        -0.5f*taille, 0, 0.5f*taille    // vertex carre gauche bas
                    };

    // Coordonnees texture
    float coordTextureTmp[] = {/*Triangle 1*/
                               0, 0,   0.5, 1,   1, 0,
                               /*Triangle 2*/
                               0, 0,   0.5, 1,   1, 0,
                               /*Triangle 3*/
                               0, 0,   0.5, 1,   1, 0,
                               /*Triangle 4*/
                               0, 0,   0.5, 1,   1, 0,
                               /*Triangle 5*/
                               0, 0,   0.5, 1,   1, 0,
                               /*Triangle 6*/
                               0, 0,   0.5, 1,   1, 0,
                               /*Triangle 7*/
                               0, 0,   0.5, 1,   1, 0,
                               /*Triangle 8*/
                               0, 0,   0.5, 1,   1, 0
                               };

    for (int i(0); i < 72; i++)
    {
        m_vertices[i] = verticesTmp[i];
    }
    for(int i(0); i<48; i++)
    {
        m_coordsTexture[i] = coordTextureTmp[i];
    }
}

Cristal::~Cristal(){}

void Cristal::afficher(mat4 &projection, mat4 &modelview)
{
    // Activation shader
    glUseProgram(m_shader.getProgramID());

        // Envoi vertices
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, m_vertices);
        glEnableVertexAttribArray(0);

        // Envoi texture
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, m_coordsTexture);
        glEnableVertexAttribArray(2);

        // Envoi matrices
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "projection"), 1, GL_FALSE, value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelview"), 1, GL_FALSE, value_ptr(modelview));

        // Verouillage texture
        glBindTexture(GL_TEXTURE_2D, m_texture.getID());

        // Rendu
        glDrawArrays(GL_TRIANGLES, 0, 24);

        // Deverouillage texture
        glBindTexture(GL_TEXTURE_2D, 0);

        // Desactivation tableaux
        glDisableVertexAttribArray(2);
        glDisableVertexAttribArray(0);

    // Desactivation shader
    glUseProgram(0);
}

float Cristal::calculRotation()
{
    // Calcul angle de rotation
    m_angle += 1.0;
    if (m_angle >= 360.0)
    {
        m_angle -= 360.0;
    }

    return m_angle;
}
