#include "Camera.h"
using namespace glm;

Camera::Camera() : m_phi(0.0), m_theta(0.0), m_orientation(), m_axeVertical(0, 0, 1), m_deplacementLateral(), m_position(), m_pointCible(), m_sensibilite(0.0), m_vitesse(0.0) {}

Camera::Camera(vec3 position, vec3 pointCible, vec3 axeVertical, float sensibilite, float vitesse) : m_phi(0.0), m_theta(0.0), m_orientation(), m_axeVertical(axeVertical), m_deplacementLateral(), m_position(position), m_pointCible(pointCible), m_sensibilite(sensibilite), m_vitesse(vitesse)
{
    // Mise a jour point cible
    setPointCible(pointCible);
}

Camera::~Camera() {}

float Camera::getSensibilite() const
{
    return m_sensibilite;
}

float Camera::getVitesse() const
{
    return m_vitesse;
}

void Camera::setSensibilite(float sensibilite)
{
    m_sensibilite = sensibilite;
}

void Camera::setVitesse(float vitesse)
{
    m_vitesse = vitesse;
}

void Camera::setPosition(vec3 position)
{
    // Mise a jour position
    m_position = position;

    // Mise a jour point cible
    m_pointCible = m_position + m_orientation;
}

void Camera::setPointCible(vec3 pointCible)
{
    // Calcul orientation
    m_orientation = m_pointCible - m_position;
    m_orientation = normalize(m_orientation);

    // Prise en compte de l'axe vertical
    if (m_axeVertical.x == 1.0)
    {
        m_phi = asin(m_orientation.x);
        m_theta = acos(m_orientation.y / cos(m_phi));
        if (m_orientation.z < 0)
        {
            m_theta *= -1;
        }
    }
    else if (m_axeVertical.y == 1.0)
    {
        m_phi = asin(m_orientation.y);
        m_theta = acos(m_orientation.z / cos(m_phi));
        if (m_orientation.z < 0)
        {
            m_theta *= -1;
        }
    }
    else
    {
        m_phi = asin(m_orientation.x);
        m_theta = acos(m_orientation.z / cos(m_phi));
        if (m_orientation.z < 0)
        {
            m_theta *= -1;
        }
    }

    // Conversion radian -> degree
    m_phi = m_phi*180 / M_PI;
    m_theta = m_theta*180 / M_PI;
}

void Camera::orienter(int xRel, int yRel)
{
    m_phi += -yRel*m_sensibilite; // -yRel car sens anti-trigo
    m_theta += -xRel*m_sensibilite;

    // Limitation angle phi (limitation sur l'axe Y pour le calcul de la normale)
    if (m_phi > 89.0)
    {
        m_phi = 89.0;
    }
    else if (m_phi < -89.0)
    {
        m_phi = -89.0;
    }

    /** --- Calcul coordonnees spheriques --- **/

    // Conversion degree -> radian
    float phiRadian = m_phi * M_PI / 180;
    float thetaRadian = m_theta * M_PI / 180;

    // Prise en compte de l'axe vertical
    if (m_axeVertical.x == 1.0)
    {
        m_orientation.x = sin(phiRadian);
        m_orientation.y = cos(phiRadian) * cos(thetaRadian);
        m_orientation.z = cos(phiRadian) * sin(thetaRadian);
    }
    else if (m_axeVertical.y == 1.0)
    {
        m_orientation.x = cos(phiRadian) * sin(thetaRadian);
        m_orientation.y = sin(phiRadian);
        m_orientation.z = cos(phiRadian) * cos(thetaRadian);
    }
    else
    {
        m_orientation.x = cos(phiRadian) * cos(thetaRadian);
        m_orientation.y = cos(phiRadian) * sin(thetaRadian);
        m_orientation.z = sin(phiRadian);
    }

    /** ------------------------------------- **/

    // Calcul normale
    m_deplacementLateral = normalize(cross(m_axeVertical, m_orientation));

    // Calcul point cible
    m_pointCible = m_position + m_orientation;

}

void Camera::deplacer(Input const &input)
{
    // Gestion orientation
    if (input.mouvementSouris())
    {
        orienter(input.getXRel(), input.getYRel());
    }

    // Camera - Avance
    if (input.getTouche(SDL_SCANCODE_UP))
    {
        m_position = m_position + m_orientation * m_vitesse;
        m_pointCible = m_position + m_orientation;
    }
    if (input.getTouche(SDL_SCANCODE_DOWN))
    {
        m_position = m_position - m_orientation * m_vitesse;
        m_pointCible = m_position + m_orientation;
    }
    if (input.getTouche(SDL_SCANCODE_LEFT))
    {
        m_position = m_position + m_deplacementLateral * m_vitesse;
        m_pointCible = m_position + m_orientation;
    }
    if (input.getTouche(SDL_SCANCODE_RIGHT))
    {
        m_position = m_position - m_deplacementLateral * m_vitesse;
        m_pointCible = m_position + m_orientation;
    }
}

void Camera::lookAt(mat4 &modelView)
{
    modelView = glm::lookAt(m_position, m_pointCible, m_axeVertical);
}
