#ifndef DEF_TEXTURE
#define DEF_TEXTURE

// Include
#ifdef WIN32
#include <GL/glew.h>

#else
#define GL3_PROTOTYPE 1
#include <GL3/gl3.h>

#endif // WIN32

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>


class Texture
{
    public:
        Texture(std::string fichierImage);
        Texture();
        Texture(Texture const &textureSource);
        Texture& operator=(Texture const &textureSource);
        ~Texture();


        bool charger();
        GLuint getID() const;
        void setFichierImage(const std::string &fichierImage);
        SDL_Surface* inverserPixels(SDL_Surface *imageSource) const;

    private:

        GLuint m_id;
        std::string m_fichierImage;
};
#endif // DEF_TEXTURE
