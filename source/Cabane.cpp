#include "Cabane.h"
using namespace glm;

Cabane::Cabane(float taille, std::string const vertexShader, std::string const fragmentShader, std::string const texture_mur, std::string const texture_toit) : m_shader(vertexShader, fragmentShader), m_texture_mur(texture_mur), m_texture_toit(texture_toit)
{
    // Chargement shader
    m_shader.charger();

    // Chargement texture
    m_texture_mur.charger();
    m_texture_toit.charger();

    taille /= 2;

    // Vertices

    float verticesTmp[] = {/* MUR FOND */
                    -taille, 0, -taille,
                        taille, 0, -taille,
                        taille, taille, -taille,

                    -taille, 0, -taille,
                        -taille, taille, -taille,
                        taille, taille, -taille,

                    /* MUR DROITE */
                    taille, 0, taille,
                        taille, 0, -taille,
                        taille, taille, -taille,

                    taille, 0, taille,
                        taille, taille, taille,
                        taille, taille, -taille,

                    /* MUR GAUCHE */
                    -taille, 0, taille,
                        -taille, 0, -taille,
                        -taille, taille, -taille,

                     -taille, 0, taille,
                        -taille, taille, taille,
                        -taille, taille, -taille,

                    /* COMBLE */
                    -taille, taille, -taille,
                        taille, taille, -taille,
                        0, taille+(taille/5.0f), -taille,

                    /* TOIT GAUCHE */
                    -taille-(taille/5.0f), taille-(taille/25.0f), -taille-(taille/5.0f),
                        -taille-(taille/5.0f), taille-(taille/25.0f), taille+(taille/5.0f),
                        0, taille+(taille/5.0f), -taille-(taille/5.0f),

                    -taille-(taille/5.0f), taille-(taille/25.0f), taille+(taille/5.0f),
                        0, taille+(taille/5.0f), -taille-(taille/5.0f),
                        0, taille+(taille/5.0f), taille+(taille/5.0f),

                    /* TOIT DROITE */
                    taille+(taille/5.0f), taille-(taille/25.0f), -taille-(taille/5.0f),
                        taille+(taille/5.0f), taille-(taille/25.0f), taille+(taille/5.0f),
                        0, taille+(taille/5.0f), -taille-(taille/5.0f),

                    taille+(taille/5.0f), taille-(taille/25.0f), taille+(taille/5.0f),
                        0, taille+(taille/5.0f), -taille-(taille/5.0f),
                        0, taille+(taille/5.0f), taille+(taille/5.0f)
                    };

    // Coordonnees texture
    float coordTextureTmp[] = {/* Mur fond */
                               0, 0,   1, 0,   1, 1,
                               0, 0,   0, 1,   1, 1,
                               /* Mur droite */
                               0, 0,   1, 0,   1, 1,
                               0, 0,   0, 1,   1, 1,
                               /* Mur gauche */
                               0, 0,   1, 0,   1, 1,
                               0, 0,   0, 1,   1, 1,
                               /* Comble */
                               0, 0,   1, 0,   0.5, 0.5,
                               /* Toit gauche */
                               0, 0,   0, 1,   1, 1,
                               0, 0,   1, 0,   1, 1,
                               /* Toit droite */
                               0, 0,   0, 1,   1, 1,
                               0, 0,   1, 0,   1, 1
                               };

    for (int i(0); i < 99; i++)
    {
        m_vertices[i] = verticesTmp[i];
    }
    for(int i(0); i<66; i++)
    {
        m_coordsTexture[i] = coordTextureTmp[i];
    }
}

Cabane::~Cabane(){}

void Cabane::afficher(mat4 &projection, mat4 &modelview)
{
    // Activation shader
    glUseProgram(m_shader.getProgramID());

        // Envoi vertices
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, m_vertices);
        glEnableVertexAttribArray(0);

        // Envoi texture
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, m_coordsTexture);
        glEnableVertexAttribArray(2);

        // Envoi matrices
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "projection"), 1, GL_FALSE, value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelview"), 1, GL_FALSE, value_ptr(modelview));

        /** Mur **/
        // Verouillage texture
        glBindTexture(GL_TEXTURE_2D, m_texture_mur.getID());

        // Rendu Mur
        glDrawArrays(GL_TRIANGLES, 0, 21);

        // Deverouillage texture
        glBindTexture(GL_TEXTURE_2D, 0);
        /** --- **/

        /** Toit **/
        // Verouillage texture
        glBindTexture(GL_TEXTURE_2D, m_texture_toit.getID());

        // Rendu
        glDrawArrays(GL_TRIANGLES, 21, 12);

        // Deverouillage texture
        glBindTexture(GL_TEXTURE_2D, 0);
        /** ---- **/

        // Desactivation tableaux
        glDisableVertexAttribArray(2);
        glDisableVertexAttribArray(0);

    // Desactivation shader
    glUseProgram(0);
}

