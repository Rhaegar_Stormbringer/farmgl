#include "Texture.h"

Texture::Texture(std::string fichierImage) : m_id(0), m_fichierImage(fichierImage){}

Texture::Texture() : m_id(0), m_fichierImage(""){}

Texture::Texture(Texture const &textureSource)
{
    m_fichierImage = textureSource.m_fichierImage;
    charger();
}

Texture& Texture::operator=(Texture const &textureSource)
{
    m_fichierImage = textureSource.m_fichierImage;
    charger();

    return *this;
}

Texture::~Texture()
{
    glDeleteTextures(1, &m_id);
}

bool Texture::charger()
{
    // Chargement image
    SDL_Surface *imageSDLTemp = IMG_Load(m_fichierImage.c_str());
    if (imageSDLTemp == 0)
    {
        std::cout << "Erreur : " << SDL_GetError() << std::endl;
        return false;
    }

    // Inversion image
    SDL_Surface *imageSDL = inverserPixels(imageSDLTemp);
    SDL_FreeSurface(imageSDLTemp);

    // Destruction eventuelle ancienne texture
    if(glIsTexture(m_id) == GL_TRUE)
    {
        glDeleteTextures(1, &m_id);
    }

    // Generation ID
    glGenTextures(1, &m_id);

    /** --- Verrouillage --- **/
    glBindTexture(GL_TEXTURE_2D, m_id);

        // Format image
        GLenum formatInterne(0);
        GLenum format(0);

        // Determination format [Image RGB]
        if (imageSDL->format->BytesPerPixel == 3)
        {
            formatInterne = GL_RGB;

            if (imageSDL->format->Rmask == 0xff)
            {
                format = GL_RGB;
            }
            else
            {
                format = GL_BGR;
            }
        }

        // Determination format [Image RGBA]
        else if (imageSDL->format->BytesPerPixel == 4)
        {
            formatInterne = GL_RGBA;

            if (imageSDL->format->Rmask == 0xff)
            {
                format = GL_RGBA;
            }
            else
            {
                format = GL_BGRA;
            }
        }

        // Autre cas : Erreur
        else
        {
            std::cout << "Erreur; format interne de l'image inconnu" << std::endl;
            SDL_FreeSurface(imageSDL);
            return false;
        }

        // Copie pixel
        glTexImage2D(GL_TEXTURE_2D, 0, formatInterne, imageSDL->w, imageSDL->h, 0, format, GL_UNSIGNED_BYTE, imageSDL->pixels);

        // Application filtres
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    /** -- Deverrouillage -- **/
    glBindTexture(GL_TEXTURE_2D, m_id);

    SDL_FreeSurface(imageSDL);
    return true;
}

GLuint Texture::getID() const
{
    return m_id;
}

void Texture::setFichierImage(const std::string &fichierImage)
{
    m_fichierImage = fichierImage;
}

SDL_Surface* Texture::inverserPixels(SDL_Surface *imageSource) const
{
    // Copie image sans les pixels
    SDL_Surface *imageInverse = SDL_CreateRGBSurface(0, imageSource->w, imageSource->h, imageSource->format->BitsPerPixel, imageSource->format->Rmask, imageSource->format->Gmask, imageSource->format->Bmask, imageSource->format->Amask);

    // Tableau temp
    unsigned char* pixelSource = (unsigned char*)imageSource->pixels;
    unsigned char* pixelInverse = (unsigned char*)imageInverse->pixels;

    // Inversion pixels
    for (int i = 0; i < imageSource->h; i++)
    {
        for (int j = 0; j < imageSource->w*imageSource->format->BytesPerPixel; j++)
        {
            pixelInverse[(imageSource->w*imageSource->format->BytesPerPixel * (imageSource->h -1 -i)) + j] = pixelSource[(imageSource->w*imageSource->format->BytesPerPixel * i) + j];
        }
    }

    return imageInverse;
}
