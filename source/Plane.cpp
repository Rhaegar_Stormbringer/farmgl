#include "Plane.h"
using namespace glm;

Plane::Plane(float taille, std::string const vertexShader, std::string const fragmentShader) : m_shader(vertexShader, fragmentShader)
{
    // Chargement shader
    m_shader.charger();

    // Tableau vertices & couleurs

    taille /= 2;

    // Vertices
    float verticesTmp[] = {/* FACE */
                    -taille, 0, -taille,
                        taille, 0, -taille,
                        taille, 0, taille,  // Triangle 1

                    -taille, 0, -taille,
                        -taille, 0, taille,
                        taille, 0, taille   // Triangle 2
                    };

    // Couleurs
    float couleursTmp[] = {/* FACE (rouge) */
                        1.0, 0.0, 0.0,
                            1.0, 0.0, 0.0,
                            1.0, 0.0, 0.0,  // Triangle 1

                        1.0, 0.0, 0.0,
                            1.0, 0.0, 0.0,
                            1.0, 0.0, 0.0  // Triangle 2
                        };

        for (int i(0); i < 18; i++)
        {
            m_vertices[i] = verticesTmp[i];
            m_couleurs[i] = couleursTmp[i];
        }
}

Plane::~Plane(){}

void Plane::afficher(mat4 &projection, mat4 &modelview)
{
    /* ------ Activation shader ------ */
    glUseProgram(m_shader.getProgramID());

        // Envoi vertices
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, m_vertices);
        glEnableVertexAttribArray(0);

        // Envoi couleur
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, m_couleurs);
        glEnableVertexAttribArray(1);

        // Envoi matrices
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "projection"), 1, GL_FALSE, value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(m_shader.getProgramID(), "modelview"), 1, GL_FALSE, value_ptr(modelview));


        // DESSIN
        glDrawArrays(GL_TRIANGLES, 0, 9);

        // Desactivation tableaux
        glDisableVertexAttribArray(1),
        glDisableVertexAttribArray(0);

    /* ---- Desactivation shader ----- */
    glUseProgram(0);
}

