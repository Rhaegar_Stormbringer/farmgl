# FarmGL

Petit projet personnel afin de valider l'apprentissage du C++ et de OpenGL.
Après une pré-modélisation sous Blender des assets, chaque objet à été codé
dans une classe différente, puis intégré à la scène finale.

![img1](screenshots/img1.png)

Outre les classes crées pour les objets - caisse, cabane, cristal, et sol, d'autres classes ont égalemnt été créées pour gérer :
- Les textures
- Les shaders
- La caméra
- La fenêtre princpale

![img2](screenshots/img2.png)

Les textures proviennent de sources libres. Elles sont chargés à l'aide de SDL, puis appliqués aux objets grâce à un tableau représentant leur coordonnées par rapport aux vertices des objets.

![img3](screenshots/img3.png) 

La caméra est totalement libre, aucune contrainte n'est appliquée sur ses axes.
- Les flèches directionnelles permettent de la déplacer dans la scène.
- La souris permet de définir le point de vue.

L'animation du cristal n'est pas une vraie animation dans le sens où le modèle n'est pas en lui-même modifié : c'est la matrice de projection sur laquelle on applique une rotation avant de rafraichir l'affichage du cristal.

![img5](screenshots/img5.gif)
